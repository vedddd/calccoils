#ifndef Q_DEBUG_STREAM_H
#define Q_DEBUG_STREAM_H

#include <QTextEdit>
#include <QPointer>

class TextEditIoDevice : public QIODevice
{
    Q_OBJECT

public:
    TextEditIoDevice(QTextEdit *const textEdit, QObject *const parent)
        : QIODevice(parent)
        , textEdit(textEdit)
    {
        open(QIODevice::WriteOnly|QIODevice::Text);
    }

protected:
    qint64 readData(char *data, qint64 maxSize) { return 0; }
    qint64 writeData(const char *data, qint64 maxSize)
    {
        if(textEdit)
        {
            textEdit->append(QString::fromUtf8(data));
        }
        return maxSize;
    }

private:
    QPointer<QTextEdit> textEdit;
};
#endif
