#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
#include <QSettings>
#include <QInputDialog>
#include <QMessageBox>
#include "../build/ui_CCmainWindow.h"
#include "../include/aboutCC.h"
#include "../include/TStranslator.h"

class mainWindow : public QMainWindow {
  Q_OBJECT
public:
  mainWindow();
  ~mainWindow();

protected:
  void changeEvent(QEvent*);

protected slots:
  void slotLanguageChanged(QAction *action);
  void slotCoilChanged(int index);
  void slotWireChanged(int index);
  void slotAboutProg();
  void slotHelpContents();

protected:
  void closeEvent(QCloseEvent *close);

private:
  Ui_MainWindow ui;

  int calcResultId;
  QSettings *settings_calccoils; // настройки программы
  QSettings *lib_coils; // библиотека каркасов катшек
  QSettings *lib_wires; // библиотека проводов
  void saveSettings();
  void loadSettings();

  void setLangGUI ();
  void createLanguageMenu();
  void loadLanguage(const QString& tLanguage);
  void switchTranslator(TSTranslator& translator, const QString& filename);
  TSTranslator translator;
  QString dirTranslations;
  QString currLang;

  void saveCoil(int index, QString text, bool replace);
  QList<QString> coilsNames;
  QList<double> coilsDiameter_Dmm;
  QList<double> coilsHeight_lmm;
  QList<double> coilsWeight_coil_g;

  void saveWire(int index, QString text, bool replace);
  QList<QString> wiresNames;
  QList<double> wiresResistivityConstant;
  QList<double> wiresSpecificGravity;
  QList<double> wiresDiameter_diamm;
  QList<double> wiresDiameter_dinsmm;

private Q_SLOTS:

  void on_pB_pwAdd_clicked(bool checked);
  void on_pB_pwDel_clicked(bool checked);
  void on_pB_pcAdd_clicked(bool checked);
  void on_pB_pcDel_clicked(bool checked);

  void on_pushButton_calc_clicked(bool checked);
  void on_pushButton_clear_clicked(bool checked);

};

#endif
