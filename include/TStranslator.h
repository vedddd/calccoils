#include <QtCore>

// TSTranslator

class TSTranslator : public QTranslator {
	Q_OBJECT

public:
	TSTranslator(QObject *parent = 0) : QTranslator(parent) {}
	~TSTranslator() {}
	bool load(const QString &filename);
	virtual QString translate(const char* context,
		const char* sourceText, const char* disambiguation = 0) const;

private:
	QMap<QString, QHash<QString, QString> > m_translate;
	void loadContext(QXmlStreamReader &xml, const QString &context);
};
