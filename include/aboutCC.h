#ifndef ABOUTCC_H
#define ABOUTCC_H

#include <QtGui>
#include "../build/ui_aboutCCdialog.h"

class Dialog : public QDialog {
  Q_OBJECT
public:
  Dialog();

private:
  Ui_Dialog ui;

private Q_SLOTS:
	void on_pushButton_web_clicked(bool checked);
	void on_pushButton_exit_clicked(bool checked);

};

#endif
