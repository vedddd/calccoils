//============================================================================
// Name        : calcInductors.h
// Author      : Vetrov D.N.
// Version     : 0.01
// Copyright   : Your copyright notice
// Description : Calculation of the inductors in C++, Ansi-style
//============================================================================
#include <cmath>

class calcInductors{
	public:
		// Расчёт числа витков у цилиндрической катушки.
		long double calcMultilayerInductors(long double diameter_d,
											long double diameter_Dc,
											long double height_l,
											long double const_kx,
											long double const_ky,
											long double *thickness_c,
											long double inductance_L);

		// Рачёт длины провода у цилиндрической катушки.
		long double lengthWireCoil(long double number_w,
								   long double diameter_d,
								   long double diameter_Dc,
								   long double height_l,
								   long double const_kx,
								   long double const_ky);

		// Расчёт сопротивления постоянному току в Ом.
		long double coilResistance(long double resistivityConstant,
								   long double length_l,
								   long double diameter_dins);

		// Расчёт веса провода в граммах.
		long double coilWeight(long double specificGravity,
							   long double length_l,
							   long double diameter_d);
};
