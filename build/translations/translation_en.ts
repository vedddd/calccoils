<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>mainWindow</name>
    <message>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <source>plans coil</source>
        <translation>plans coil</translation>
    </message>
    <message>
        <source>Help Contents</source>
        <translation>Help Contents</translation>
    </message>
    <message>
        <source>About CalcCoils</source>
        <translation>About CalcCoils</translation>
    </message>
    <message>
    	<source>parameters wire</source>
    	<translation>parameters wire</translation>
	</message>
    <message>
    	<source>resistivity</source>
		<translation>resistivity</translation>
	</message>
    <message>
    	<source>density</source>
		<translation>density</translation>
	</message>
	<message>
    	<source>mm</source>
		<translation>mm</translation>
	</message>
	<message>
    	<source>m</source>
		<translation>m</translation>
	</message>
	<message>
    	<source>Om</source>
		<translation>Om</translation>
	</message>
	<message>
    	<source>Om*mm^2/m</source>
		<translation>Om*mm^2/m</translation>
	</message>
	<message>
    	<source>kg/m^3</source>
		<translation>kg/m^3</translation>
	</message>
	<message>
    	<source>parameters coil</source>
		<translation>parameters coil</translation>
	</message>
	<message>
    	<source>g</source>
		<translation>g</translation>
	</message>
	<message>
    	<source>winding density</source>
		<translation>winding density</translation>
	</message>
	<message>
    	<source>inductance</source>
		<translation>inductance</translation>
	</message>
	<message>
    	<source>mH</source>
		<translation>mH</translation>
	</message>
	<message>
    	<source>calculate</source>
		<translation>calculate</translation>
	</message>
	<message>
    	<source>results calculation</source>
		<translation>results calculation</translation>
	</message>
	<message>
    	<source>results clear</source>
		<translation>results clear</translation>
	</message>
	<message>
    	<source>Error!</source>
		<translation>Error!</translation>
	</message>
	<message>
    	<source>number of calculation: </source>
		<translation>number of calculation: </translation>
	</message>
	<message>
    	<source>number of the turns  W = </source>
		<translation>number of the turns  W = </translation>
	</message>
	<message>
    	<source>largest number of turns in a layer  Wl = </source>
		<translation>largest number of turns in a layer  Wl = </translation>
	</message>
	<message>
    	<source>number of layers  N = </source>
		<translation>number of layers  N = </translation>
	</message>
	<message>
    	<source>length of the  wire  l = </source>
		<translation>length of the  wire  l = </translation>
	</message>
	<message>
    	<source>resistance of the wire  R = </source>
		<translation>resistance of the wire  R = </translation>
	</message>
	<message>
    	<source>weight of the wire  m = </source>
		<translation>weight of the wire  m = </translation>
	</message>
	<message>
    	<source>weight of the coil  M = </source>
		<translation>weight of the coil  M = </translation>
	</message>
	<message>
    	<source>thickness of the winding  c > </source>
		<translation>thickness of the winding  c > </translation>
	</message>
	<message>
    	<source>external diameter of the coil  De > </source>
		<translation>external diameter of the coil  De > </translation>
	</message>
	<message>
    	<source>Name of the coil</source>
		<translation>Name of the coil</translation>
	</message>
	<message>
    	<source>Enter the name of the coil:</source>
		<translation>Enter the name of the coil:</translation>
	</message>
	<message>
    	<source>Question</source>
		<translation>Question</translation>
	</message>
	<message>
    	<source>The name '%1' already exists. Replace?</source>
		<translation>The name '%1' already exists. Replace?</translation>
	</message>
	<message>
    	<source>Are you sure you want to delete '%1' from the list?</source>
		<translation>Are you sure you want to delete '%1' from the list?</translation>
	</message>
	<message>
    	<source>Name of the wire</source>
		<translation>Name of the wire</translation>
	</message>
	<message>
    	<source>Enter the name of the wire:</source>
		<translation>Enter the name of the wire:</translation>
	</message>
	<message>
    	<source>Information</source>
		<translation>Information</translation>
	</message>
	<message>
    	<source>File 'CalcCoilsDoc_%1.pdf' not found!</source>
		<translation>File 'CalcCoilsDoc_%1.pdf' not found!</translation>
	</message>
</context>
<context>
	<name>Dialog</name>
    <message>
        <source>About CalcCoils</source>
        <translation>About CalcCoils</translation>
    </message>
    <message>
        <source>The program for calculation of inductors.</source>
        <translation>The program for calculation of inductors.</translation>
    </message>
    <message>
        <source>CalcCoils is free software.</source>
        <translation>CalcCoils is free software.</translation>
    </message>
</context>
</TS>