<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>mainWindow</name>
    <message>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <source>plans coil</source>
        <translation>чертёж катушки</translation>
    </message>
    <message>
        <source>Help Contents</source>
        <translation>Содержание справки</translation>
    </message>
    <message>
        <source>About CalcCoils</source>
        <translation>О программе CalcCoils</translation>
    </message>
    <message>
    	<source>parameters wire</source>
    	<translation>параметры провода</translation>
	</message>
    <message>
    	<source>resistivity</source>
		<translation>уд. сопр.</translation>
	</message>
    <message>
    	<source>density</source>
		<translation>плотность</translation>
	</message>
	<message>
    	<source>mm</source>
		<translation>мм</translation>
	</message>
	<message>
    	<source>m</source>
		<translation>м</translation>
	</message>
	<message>
    	<source>Om</source>
		<translation>Ом</translation>
	</message>
	<message>
    	<source>Om*mm^2/m</source>
		<translation>Ом*мм^2/м</translation>
	</message>
	<message>
    	<source>kg/m^3</source>
		<translation>кг/м^3</translation>
	</message>
	<message>
    	<source>parameters coil</source>
		<translation>параметры катушки</translation>
	</message>
	<message>
    	<source>g</source>
		<translation>г</translation>
	</message>
	<message>
    	<source>winding density</source>
		<translation>плотность намотки</translation>
	</message>
	<message>
    	<source>inductance</source>
		<translation>индуктивность</translation>
	</message>
	<message>
    	<source>mH</source>
		<translation>мГн</translation>
	</message>
	<message>
    	<source>calculate</source>
		<translation>рассчитать</translation>
	</message>
	<message>
    	<source>results calculation</source>
		<translation>результаты расчёта</translation>
	</message>
	<message>
    	<source>results clear</source>
		<translation>очистить результаты</translation>
	</message>
		<message>
    	<source>Error!</source>
		<translation>Ошибка!</translation>
	</message>
	<message>
    	<source>number of calculation: </source>
		<translation>номер расчёта: </translation>
	</message>
	<message>
    	<source>number of the turns  W = </source>
		<translation>число витков  W = </translation>
	</message>
	<message>
    	<source>largest number of turns in a layer  Wl = </source>
		<translation>наибольшее число витков в слое  Wl = </translation>
	</message>
	<message>
    	<source>number of layers  N = </source>
		<translation>число слоёв  N = </translation>
	</message>
	<message>
    	<source>length of the  wire  l = </source>
		<translation>длина провода  l = </translation>
	</message>
	<message>
    	<source>resistance of the wire  R = </source>
		<translation>сопротивление провода  R = </translation>
	</message>
	<message>
    	<source>weight of the wire  m = </source>
		<translation>вес провода  m = </translation>
	</message>
	<message>
    	<source>weight of the coil  M = </source>
		<translation>вес катушки  M = </translation>
	</message>
	<message>
    	<source>thickness of the winding  c > </source>
		<translation>толщина обмотки  c > </translation>
	</message>
	<message>
    	<source>external diameter of the coil  De > </source>
		<translation>внешний диаметр катушки  De > </translation>
	</message>
	<message>
    	<source>Name of the coil</source>
		<translation>Имя катушки</translation>
	</message>
	<message>
    	<source>Enter the name of the coil:</source>
		<translation>Введите имя катушки:</translation>
	</message>
	<message>
    	<source>Question</source>
		<translation>Вопрос</translation>
	</message>
	<message>
    	<source>The name '%1' already exists. Replace?</source>
		<translation>Имя '%1' уже существует. Заменить?</translation>
	</message>
	<message>
    	<source>Are you sure you want to delete '%1' from the list?</source>
		<translation>Вы хотите удалить '%1' из списка?</translation>
	</message>
	<message>
    	<source>Name of the wire</source>
		<translation>Имя провода</translation>
	</message>
	<message>
    	<source>Enter the name of the wire:</source>
		<translation>Введите имя провода:</translation>
	</message>
	<message>
    	<source>Information</source>
		<translation>Информация</translation>
	</message>
	<message>
    	<source>File 'CalcCoilsDoc_%1.pdf' not found!</source>
		<translation>Файл 'CalcCoilsDoc_%1.pdf' не существует!</translation>
	</message>
</context>
<context>
	<name>Dialog</name>
    <message>
        <source>About CalcCoils</source>
        <translation>О программе CalcCoils</translation>
    </message>
    <message>
        <source>The program for calculation of inductors.</source>
        <translation>Программа рассчитывает катушки индуктивности.</translation>
    </message>
    <message>
        <source>CalcCoils is free software.</source>
        <translation>CalcCoils является свободным ПО.</translation>
    </message>
</context>
</TS>