#include "../include/TStranslator.h"

// Helper function
void xmlSkipElement(QXmlStreamReader &xml)
{
	int level = 0;
	while (!xml.atEnd()) {
		xml.readNext();
		if (xml.isStartElement()) level++;
		else if (xml.isEndElement())
		{
			level--;
			if (level < 0) return;
		}
	}
}

/******************************************************************************
** TSTranslator
******************************************************************************/

bool TSTranslator::load(const QString &filename)
{
	QFile file(filename);
	if (!file.open(QIODevice::ReadOnly)) return false;

	m_translate.clear();

	QXmlStreamReader xml(&file);
	while (!xml.atEnd())
	{
		xml.readNext();
		if (xml.isStartElement() && xml.name() == "TS")
		{
			while (!xml.atEnd())
			{
				xml.readNext();
				if (xml.isEndElement()) break;	// </TS>
				if (xml.isStartElement() && xml.name() == "context")
				{
					while (!xml.atEnd())
					{
						xml.readNext();
						if (xml.isEndElement()) break;	// </context>
						if (xml.isStartElement() && xml.name() == "name")
						{
							loadContext(xml, xml.readElementText());
							break;
						} else if (xml.isStartElement()) xmlSkipElement(xml);
					}
				} else if (xml.isStartElement()) xmlSkipElement(xml);
			}
		} else if (xml.isStartElement()) xmlSkipElement(xml);
	}

	if (xml.hasError())
	{
		qDebug() << QString("%1 at line %2 column %3").arg(
			xml.errorString()).arg(xml.lineNumber()).arg(xml.columnNumber());
	}

	return !xml.hasError();
}

void TSTranslator::loadContext(QXmlStreamReader &xml, const QString &context)
{
	while (!xml.atEnd())
	{
		xml.readNext();
		if (xml.isEndElement()) break; // </context>
		if (xml.isStartElement() && xml.name() == "message")
		{
			bool numerus = xml.attributes().value("numerus").compare(
				"yes", Qt::CaseInsensitive) == 0;
			QString source, translation;

			while (!xml.atEnd())
			{
				xml.readNext();
				if (xml.isEndElement()) break; // </message>
				if (xml.isStartElement())
				{
					if (xml.name() == "source") {
						source = xml.readElementText();
					} else if (xml.name() == "translation") {
						if (!numerus) translation = xml.readElementText();
						else
						{
							while (!xml.atEnd())
							{
								xml.readNext();
								if (xml.isEndElement()) break; // </translation>
								if (xml.isStartElement() && xml.name() == "numerusform")
								{
									translation = xml.readElementText();
									xmlSkipElement(xml);
									break;
								} else if (xml.isStartElement()) xmlSkipElement(xml);
							}
						}
					} else xmlSkipElement(xml);
				}
			}

			if (!source.isEmpty() && !translation.isEmpty())
				m_translate[context].insert(source, translation);

		} else if (xml.isStartElement()) xmlSkipElement(xml);
	}
}

QString TSTranslator::translate(const char* context,
		const char* sourceText, const char* disambiguation) const
{
	if (m_translate.value(context).contains(sourceText))
		return m_translate.value(context).value(sourceText);
	else
		return sourceText;
}
