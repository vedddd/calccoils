#include <QApplication>
#include <QtDebug>
#include "../include/mainWindow.h"

int main(int argc, char *argv[]){
	qDebug ("Application initialized");

	QApplication app(argc, argv);

	app.setWindowIcon(QIcon(":images/icon.png")); // иконка приложения

    mainWindow mainwnd;
	mainwnd.show();

	int ret = app.exec ();
	qDebug ("Application closed with code %d", ret);
	return ret;
}
