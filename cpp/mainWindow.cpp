#include <QtGui>
#include <QtCore>
#include <QtDebug>
#include <QTextCodec>
#include <cmath>
#include <iostream>
#include "../include/mainWindow.h"
#include "../include/qDebugStream.h"
#include "../include/calcInductors.h"

mainWindow::mainWindow ():calcResultId(1) {
	ui.setupUi(this);
	this->setWindowTitle("CalcCoils");

	// Загрузка иконок для кнопок
	QPixmap pixItem;
	pixItem.load(":images/plansMultiCoil.png");
	ui.label_plansCoil->setPixmap(pixItem);

	QSize iconSize(16, 16);
	QPixmap pixIconSave;
	pixIconSave.load(":images/pb_save.png");
	ui.pB_pwAdd->setIconSize(iconSize);
	ui.pB_pcAdd->setIconSize(iconSize);
	ui.pB_pwAdd->setIcon(pixIconSave);
	ui.pB_pcAdd->setIcon(pixIconSave);

	QPixmap pixIconDel;
	pixIconDel.load(":images/pb_delete.png");
	ui.pB_pwDel->setIconSize(iconSize);
	ui.pB_pcDel->setIconSize(iconSize);
	ui.pB_pwDel->setIcon(pixIconDel);
	ui.pB_pcDel->setIcon(pixIconDel);

	ui.QMenu_helpContents->setIcon(QIcon(":images/help.png"));

	// Инициализация сигналов и слотов
	// Вызов контекстной справки
	ui.QMenu_helpContents->setShortcut(Qt::Key_F1);
	connect(ui.QMenu_helpContents, SIGNAL(triggered()), this, SLOT(slotHelpContents()));
	// Вызов окна с информацией о программе
	connect(ui.QMenu_aboutProg, SIGNAL(triggered()), this, SLOT(slotAboutProg()));
	// Выбор параметров катушки из списка
	connect(ui.comboBox_pcSet, SIGNAL(activated(int)), this, SLOT(slotCoilChanged(int)));
	// Выбор параметров провода из списка
	connect(ui.comboBox_pwSet, SIGNAL(activated(int)), this, SLOT(slotWireChanged(int)));

	// Загрузка настроек
	settings_calccoils = new QSettings("settings.conf",QSettings::IniFormat);
	lib_coils = new QSettings("libraries/lib_coils.libcc",QSettings::IniFormat);
	lib_wires = new QSettings("libraries/lib_wires.libcc",QSettings::IniFormat);
	loadSettings();

	// Загрузка языка
	dirTranslations = "translations";
	createLanguageMenu();
}

// Добавление нового провода
void mainWindow::on_pB_pwAdd_clicked(bool checked) {
	bool ok;
	bool replace = false;
	int numRep;
	QString replaceName = ui.comboBox_pwSet->currentText();
	QString text = QInputDialog::getText(this, tr("Name of the wire"),
	                                         tr("Enter the name of the wire:"), QLineEdit::Normal,
	                                         replaceName, &ok);
	if (ok && !text.isEmpty()) {
		int lenList = QStringList(wiresNames).length();
		for(int i=0;i<lenList;i++) {
			if (wiresNames[i] == text) {
				replace = true;
				numRep = i;
			}
		}
		if (replace) {
			int ret = QMessageBox::question(this, tr("Question"),
					tr("The name '%1' already exists. Replace?").arg(text),
					QMessageBox::Yes, QMessageBox::No);
			switch (ret) {
				case QMessageBox::Yes: {
					saveWire(numRep, text, true);
				}
					break;
			}
		}
		else
			saveWire(lenList, text, false);
	}
}

// Сохраняем или перезаписываем провод
void  mainWindow::saveWire(int index, QString text, bool replace) {
	if (replace) {
		// изменяем старый провод
		// удельное сопротивление
		wiresResistivityConstant[index] = ui.doubleSpinBox_pwResist->value();
		// удельная плотность
		wiresSpecificGravity[index] = ui.doubleSpinBox_pwDens->value();
		// диаметр провода с изоляцией
		wiresDiameter_diamm[index] = ui.doubleSpinBox_pvd->value();
		// диаметр провода без изоляции
		wiresDiameter_dinsmm[index] = ui.doubleSpinBox_pvd_->value();
	} else {
		// создаём новый каркас
		ui.comboBox_pwSet->addItem(text);
		// название каркаса
		wiresNames.append(text);
		// удельное сопротивление
		wiresResistivityConstant.append(ui.doubleSpinBox_pwResist->value());
		// удельная плотность
		wiresSpecificGravity.append(ui.doubleSpinBox_pwDens->value());
		// диаметр провода с изоляцией
		wiresDiameter_diamm.append(ui.doubleSpinBox_pvd->value());
		// диаметр провода без изоляции
		wiresDiameter_dinsmm.append(ui.doubleSpinBox_pvd_->value());
	}
	// задаём положение провода в список
	ui.comboBox_pwSet->setCurrentIndex(index);

	// сохраняем провод в библиотеку
	lib_wires->setValue(QString("%1/resistivityConstant").arg(wiresNames[index]),
									ui.doubleSpinBox_pwResist->value());
	lib_wires->setValue(QString("%1/specificGravity").arg(wiresNames[index]),
										ui.doubleSpinBox_pwDens->value());
	lib_wires->setValue(QString("%1/diameter_diamm").arg(wiresNames[index]),
										ui.doubleSpinBox_pvd->value());
	lib_wires->setValue(QString("%1/diameter_dinsmm").arg(wiresNames[index]),
										ui.doubleSpinBox_pvd_->value());
	lib_wires->sync(); //записываем настройки*/;
}

// Удаление провода
void mainWindow::on_pB_pwDel_clicked(bool checked) {
	if (!QString(ui.comboBox_pwSet->currentText()).isEmpty()) {
		int ret = QMessageBox::question(this, tr("Question"),
										tr("Are you sure you want to delete '%1' from the list?")
										.arg(ui.comboBox_pwSet->currentText()),
										QMessageBox::Yes, QMessageBox::No);
		switch (ret) {
			case QMessageBox::Yes: {
				int index = ui.comboBox_pwSet->currentIndex();
				// удаляем провод из библиотеки
				lib_wires->remove(wiresNames[index]);
				// удаляем все параметры провода
				wiresNames.removeAt(index);
				wiresResistivityConstant.removeAt(index);
				wiresSpecificGravity.removeAt(index);
				wiresDiameter_diamm.removeAt(index);
				wiresDiameter_dinsmm.removeAt(index);
				// удаляем каркас из списка
				ui.comboBox_pwSet->removeItem(index);

				// изменяем параметры провода
				if (index-1 < 0) index = 0;
				else index = index-1;
				ui.comboBox_pwSet->setCurrentIndex(index);
				ui.doubleSpinBox_pwResist->setValue(wiresResistivityConstant[index]);
				ui.doubleSpinBox_pwDens->setValue(wiresSpecificGravity[index]);
				ui.doubleSpinBox_pvd->setValue(wiresDiameter_diamm[index]);
				ui.doubleSpinBox_pvd_->setValue(wiresDiameter_dinsmm[index]);
			}
				break;
		}
	}
}

// Обработка события при выборе провода из списка
void mainWindow::slotWireChanged(int index) {
	ui.doubleSpinBox_pwResist->setValue(wiresResistivityConstant[index]);
	ui.doubleSpinBox_pwDens->setValue(wiresSpecificGravity[index]);
	ui.doubleSpinBox_pvd->setValue(wiresDiameter_diamm[index]);
	ui.doubleSpinBox_pvd_->setValue(wiresDiameter_dinsmm[index]);
}

// Добавление нового каркаса
void mainWindow::on_pB_pcAdd_clicked(bool checked) {
	bool ok;
	bool replace = false;
	int numRep;
	QString replaceName = ui.comboBox_pcSet->currentText();
	QString text = QInputDialog::getText(this, tr("Name of the coil"),
	                                         tr("Enter the name of the coil:"), QLineEdit::Normal,
	                                         replaceName, &ok);
	if (ok && !text.isEmpty()) {
		int lenList = QStringList(coilsNames).length();
		for(int i=0;i<lenList;i++) {
			if (coilsNames[i] == text) {
				replace = true;
				numRep = i;
			}
		}
		if (replace) {
			int ret = QMessageBox::question(this, tr("Question"),
					tr("The name '%1' already exists. Replace?").arg(text),
					QMessageBox::Yes, QMessageBox::No);
			switch (ret) {
				case QMessageBox::Yes: {
					saveCoil(numRep, text, true);
				}
					break;
			}
		}
		else
			saveCoil(lenList, text, false);
	}
}

// Сохраняем или перезаписываем каркас катушки
void  mainWindow::saveCoil(int index, QString text, bool replace) {
	if (replace) {
		// изменяем старый каркас
		// внутренний диаметр
		coilsDiameter_Dmm[index] = ui.doubleSpinBox_pcD->value();
		// ширина намотки
		coilsHeight_lmm[index] = ui.doubleSpinBox_pcL->value();
		// вес катушки
		coilsWeight_coil_g[index] = ui.doubleSpinBox_pcMc->value();
	} else {
		// создаём новый каркас
		ui.comboBox_pcSet->addItem(text);
		// название каркаса
		coilsNames.append(text);
		// внутренний диаметр
		coilsDiameter_Dmm.append(ui.doubleSpinBox_pcD->value());
		// ширина намотки
		coilsHeight_lmm.append(ui.doubleSpinBox_pcL->value());
		// вес катушки
		coilsWeight_coil_g.append(ui.doubleSpinBox_pcMc->value());
	}
	// задаём положение новой катушке в список
	ui.comboBox_pcSet->setCurrentIndex(index);

	// сохраняем каркас в библиотеку
	lib_coils->setValue(QString("%1/diameter_Dmm").arg(coilsNames[index]),
								ui.doubleSpinBox_pcD->value());
	lib_coils->setValue(QString("%1/height_lmm").arg(coilsNames[index]),
						ui.doubleSpinBox_pcL->value());
	lib_coils->setValue(QString("%1/weight_coil_g").arg(coilsNames[index]),
						ui.doubleSpinBox_pcMc->value());
	lib_coils->sync(); //записываем настройки*/;
}

// Удаление каркаса
void mainWindow::on_pB_pcDel_clicked(bool checked) {
	if (!QString(ui.comboBox_pcSet->currentText()).isEmpty()) {
		int ret = QMessageBox::question(this, tr("Question"),
										tr("Are you sure you want to delete '%1' from the list?")
										.arg(ui.comboBox_pcSet->currentText()),
										QMessageBox::Yes, QMessageBox::No);
		switch (ret) {
			case QMessageBox::Yes: {
				int index = ui.comboBox_pcSet->currentIndex();
				// удаляем каркас из библиотеки
				lib_coils->remove(coilsNames[index]);
				// удаляем все параметры каркаса
				coilsNames.removeAt(index);
				coilsDiameter_Dmm.removeAt(index);
				coilsHeight_lmm.removeAt(index);
				coilsWeight_coil_g.removeAt(index);
				// удаляем каркас из списка
				ui.comboBox_pcSet->removeItem(index);

				// изменяем параметры катушки
				if (index-1 < 0) index = 0;
				else index = index-1;
				ui.comboBox_pcSet->setCurrentIndex(index);
				ui.doubleSpinBox_pcD->setValue(coilsDiameter_Dmm[index]);
				ui.doubleSpinBox_pcL->setValue(coilsHeight_lmm[index]);
				ui.doubleSpinBox_pcMc->setValue(coilsWeight_coil_g[index]);
			}
				break;
		}
	}
}

// Обработка события при выборе катушки из списка
void mainWindow::slotCoilChanged(int index) {
	ui.doubleSpinBox_pcD->setValue(coilsDiameter_Dmm[index]); // внутренний диаметр катушки в миллиметрах
	ui.doubleSpinBox_pcL->setValue(coilsHeight_lmm[index]); // ширина намотки катушки в миллиметрах
	ui.doubleSpinBox_pcMc->setValue(coilsWeight_coil_g[index]); // вес катушки без провода в граммах
}

// Динамическое создание и выбор пункта меню в зависимости от перевода
void mainWindow::createLanguageMenu() {
    QActionGroup* langGroup = new QActionGroup(ui.QMenu_lang);
    langGroup->setExclusive(true);
    connect(langGroup, SIGNAL(triggered(QAction *)), this, SLOT(slotLanguageChanged(QAction *)));

    // Определение системного языка
    QString defaultLocale = QLocale::system().name();       // "ru_RU"
    defaultLocale.truncate(defaultLocale.lastIndexOf('_')); // "ru"

    QDir dir(dirTranslations);
    QStringList fileNames = dir.entryList(QStringList("translation_*.ts"));

    // Создание и выбор локации из названия файла
    for (int i = 0; i < fileNames.size(); ++i) {
        QString locale;
        locale = fileNames[i];                  // "translation_ru.ts"
        locale.truncate(locale.lastIndexOf('.'));   // "translation_ru"
        locale.remove(0, locale.indexOf('_') + 1);   // "ru"

        QString lang = QLocale::languageToString(QLocale(locale).language());
        QIcon ico(QString("%1/%2.png").arg(dirTranslations).arg(locale));

        QAction *action = new QAction(ico, lang, this);
        action->setCheckable(true);
        action->setData(locale);

        ui.QMenu_lang->addAction(action);
        langGroup->addAction(action);

        // Выбор транслятора и языка по умолчанию
        if (locale == currLang) {
        	switchTranslator(translator, QString("translation_%1.ts").arg(currLang));
        	action->setChecked(true);
        }
		if (defaultLocale == locale && QString(currLang).isEmpty()){
			switchTranslator(translator, QString("translation_%1.ts").arg(locale));
			action->setChecked(true);
		}
    }
}

// Вызывается каждый раз когда нажимается пункт в меню "Язык".
void mainWindow::slotLanguageChanged(QAction* action) {
    if(0 != action) {
        loadLanguage(action->data().toString());
    }
}

// Загружаем транслятор
void mainWindow::loadLanguage(const QString& tLanguage) {
    if(currLang != tLanguage) {
        currLang = tLanguage;
        switchTranslator(translator, QString("translation_%1.ts").arg(tLanguage));
    }
}

void mainWindow::switchTranslator(TSTranslator& translator, const QString& filename) {
	translator.load(QString("%1/%2").arg(dirTranslations).arg(filename));
	qApp->installTranslator(&translator);
	setLangGUI ();
}

void mainWindow::changeEvent(QEvent* event) {
    if(0 != event) {
        switch(event->type()) {
        // this event is send if a translator is loaded
        //case QEvent::LanguageChange:
            //ui.retranslateUi(this);
            //break;
        // this event is send, if the system, language changes
        case QEvent::LocaleChange: {
                QString locale = QLocale::system().name();
                locale.truncate(locale.lastIndexOf('_'));
                loadLanguage(locale);
            }
            break;
        }
    }
    QMainWindow::changeEvent(event);
}

void mainWindow::closeEvent(QCloseEvent *close) {
	saveSettings();
}

// Элементы GUI для превода
void mainWindow::setLangGUI () {
	ui.QMenu_lang->setTitle(tr("Language"));
	ui.QMenu_help->setTitle(tr("Help"));
	ui.QMenu_helpContents->setText(tr("Help Contents"));
	ui.QMenu_aboutProg->setText(tr("About CalcCoils"));

	ui.groupBox_image->setTitle(tr("plans coil"));

	ui.groupBox_pr->setTitle(tr("parameters wire"));
	ui.label_res->setText(tr("resistivity"));
	ui.label_Den->setText(tr("density"));
	ui.label_pvd->setText(tr("mm"));
	ui.label_pwd_->setText(tr("mm"));
	ui.label_pwResist->setText(tr("Om*mm^2/m"));
	ui.label_pwDens->setText(tr("kg/m^3"));

	ui.groupBox_pc->setTitle(tr("parameters coil"));
	ui.label_pcD->setText(tr("mm"));
	ui.label_pcL->setText(tr("mm"));
	ui.label_pcL_2->setText(tr("g"));

	ui.groupBox_wd->setTitle(tr("winding density"));

	ui.groupBox_induc->setTitle(tr("inductance"));
	ui.label_pcH->setText(tr("mH"));

	ui.pushButton_calc->setText(tr("calculate"));
	ui.groupBox_resCal->setTitle(tr("results calculation"));
	ui.pushButton_clear->setText(tr("results clear"));
}

void mainWindow::loadSettings() {
	// параметры окна
	this->restoreGeometry(settings_calccoils->value("settings/mainWindowGeometry").toByteArray());
	this->restoreState(settings_calccoils->value("settings/mainWindowState").toByteArray());

	// параметры языка
	currLang = (settings_calccoils->value("settings/language").toString());

	// загрузка параметров проводов из библиотеки
	QStringList namesW = lib_wires->childGroups();
	for(int i=0;i<namesW.length();i++){
		// название провода
		wiresNames.append(namesW[i]);
		// удельное сопротивление
		wiresResistivityConstant.append(lib_wires->value(QString("%1/resistivityConstant")
				.arg(namesW[i])).toDouble());
		// удельная плотность
		wiresSpecificGravity.append(lib_wires->value(QString("%1/specificGravity")
				.arg(namesW[i])).toDouble());
		// диаметр провода с изоляцией
		wiresDiameter_diamm.append(lib_wires->value(QString("%1/diameter_diamm")
				.arg(namesW[i])).toDouble());
		// диаметр провода без изоляции
		wiresDiameter_dinsmm.append(lib_wires->value(QString("%1/diameter_dinsmm")
				.arg(namesW[i])).toDouble());

		// вводим название нового провода в список
		ui.comboBox_pwSet->addItem(wiresNames[i]);
	}

	// выбор параметров провода
	int numberWire = abs(settings_calccoils->value("settings/numbWire").value<int>());
	if (numberWire <= QStringList(wiresNames).length()-1
			&& QStringList(namesW).length()>0) {
		ui.doubleSpinBox_pwResist->setValue(wiresResistivityConstant[numberWire]);
		ui.doubleSpinBox_pwDens->setValue(wiresSpecificGravity[numberWire]);
		ui.doubleSpinBox_pvd->setValue(wiresDiameter_diamm[numberWire]);
		ui.doubleSpinBox_pvd_->setValue(wiresDiameter_dinsmm[numberWire]);
		// задаём положение в списке
		ui.comboBox_pwSet->setCurrentIndex(numberWire);
	}

	// загрузка параметров катушек из библиотеки
	QStringList namesC = lib_coils->childGroups();
	for(int i=0;i<namesC.length();i++){
		// название каркаса
		coilsNames.append(namesC[i]);
		// внутренний диаметр
		coilsDiameter_Dmm.append(lib_coils->value(QString("%1/diameter_Dmm").arg(namesC[i])).toDouble());
		// ширина намотки
		coilsHeight_lmm.append(lib_coils->value(QString("%1/height_lmm").arg(namesC[i])).toDouble());
		// вес катушки
		coilsWeight_coil_g.append(lib_coils->value(QString("%1/weight_coil_g").arg(namesC[i])).toDouble());

		// вводим название новой катушки в список
		ui.comboBox_pcSet->addItem(coilsNames[i]);
	}

	// выбор параметров катушки
	int numberCoil = abs(settings_calccoils->value("settings/numbCoil").value<int>());
	if (numberCoil <= QStringList(coilsNames).length()-1
			&& QStringList(namesC).length()>0) {
		ui.doubleSpinBox_pcD->setValue(coilsDiameter_Dmm[numberCoil]);
		ui.doubleSpinBox_pcL->setValue(coilsHeight_lmm[numberCoil]);
		ui.doubleSpinBox_pcMc->setValue(coilsWeight_coil_g[numberCoil]);
		// задаём положение в списке
		ui.comboBox_pcSet->setCurrentIndex(numberCoil);
	}

	// плотности намотки kx и ky
	ui.doubleSpinBox_kx->setValue(settings_calccoils->value("settings/const_kx")
			.value<double>());
	ui.doubleSpinBox_ky->setValue(settings_calccoils->value("settings/const_ky")
			.value<double>());

	// индуктивность в милиГенри
	ui.doubleSpinBox_pcH->setValue(settings_calccoils->value("settings/inductance_mL")
				.value<double>());
}

// Сохраниение настроек.
void mainWindow::saveSettings() {
	// параметры окна
	settings_calccoils->setValue("settings/mainWindowGeometry", saveGeometry());
	settings_calccoils->setValue("settings/mainWindowState", saveState());

	// параметры языка
	settings_calccoils->setValue("settings/language", currLang);

	// сохранение параметров провода по номеру из списка
	settings_calccoils->setValue("settings/numbWire", abs(ui.comboBox_pwSet->currentIndex()));

	// сохранение параметров катушки по номеру из списка
	settings_calccoils->setValue("settings/numbCoil", abs(ui.comboBox_pcSet->currentIndex()));

	// сохранение параметров плотности намотки
	settings_calccoils->setValue("settings/const_kx", ui.doubleSpinBox_kx->value());
	settings_calccoils->setValue("settings/const_ky", ui.doubleSpinBox_ky->value());

	// индуктивность в милиГенри
	settings_calccoils->setValue("settings/inductance_mL", ui.doubleSpinBox_pcH->value());
	settings_calccoils->sync(); //записываем настройки
}

void mainWindow::on_pushButton_calc_clicked(bool checked) {
	// Инициализация вывода.
	QTextStream  cout(new TextEditIoDevice(ui.resultstBrowser, this));
	cout.setCodec("UTF-8");

	// Постоянные материала провода:
	long double resistivityConstant; // удельное сопротивление провода Ом*мм^2/м;
	long double specificGravity; // плотность провода кг/м^3
	long double diameter_dinsmm; // диаметр провода без изоляции мм
	long double weight_coil; // вес катушки без провода в граммах
	resistivityConstant = ui.doubleSpinBox_pwResist->value();
	specificGravity = ui.doubleSpinBox_pwDens->value();
	diameter_dinsmm = ui.doubleSpinBox_pvd_->value();
	weight_coil = ui.doubleSpinBox_pcMc->value();

	// Основные параметры:
	long double diameter_dmm; // толщина провода в миллиметрах
	long double diameter_Dcmm; // внутренний диаметр катушки в миллиметрах
	long double height_lmm; // ширина намотки катушки в миллиметрах
	long double inductance_mL; // индуктивность в милиГенри
	diameter_dmm = ui.doubleSpinBox_pvd->value();
	diameter_Dcmm = ui.doubleSpinBox_pcD->value();
	height_lmm = ui.doubleSpinBox_pcL->value();
	inductance_mL = ui.doubleSpinBox_pcH->value();

	// Коэффициенты:
	long double const_kx = ui.doubleSpinBox_kx->value(); // коэф. задающий плотность намотки между слоями (d*const_kx)
	long double const_ky = ui.doubleSpinBox_ky->value(); // коэф. задающий плотность намотки между витками (d*const_ky)

	// Вычисляемые величины:
	long double diameter_dins = diameter_dinsmm/1000; // диаметр провода без изоляции (лака)
	long double diameter_d = diameter_dmm/1000; // толщина провода в метрах
	long double diameter_Dc = diameter_Dcmm/1000; // внутренний диаметр катушки в метрах
	long double height_l = height_lmm/1000; // ширина намотки катушки в метрах
	long double inductance_L = inductance_mL/1000; // индуктивность в Генри
	long double number_w = 0; // число витков многослойной катушки
	long double length_l = 0; // длина провода в метрах
	long double resistance = 0; // сопротивление провода в Омах
	long double weight = 0; // вес провода в килограммах

	long double thickness_c = height_l; // предполагаемая толщина намотки в метрах

	QString messError = tr("Error!");
	QString separator = "----------------------------------------------------------";
	// Проверка ошибок в заданных данных
	if (diameter_d <= diameter_dins) {
		cout << messError << '\n' << tr("d' >= d") << '\n' << separator << endl;
		return;
	}
	if (2*const_ky*diameter_d > height_l) {
		cout << messError << '\n' << tr("2*Ky*d > L") << '\n' << separator << endl;
		return;
	}

	calcInductors coil;
	// Расчёт числа витков и толщины намотки многослойной катушки.
	number_w = coil.calcMultilayerInductors( diameter_d, diameter_Dc, height_l, const_kx,
											 const_ky, &thickness_c, inductance_L );

	// Расчёт длины провода многослойной катушки в метрах.
	length_l = coil.lengthWireCoil( number_w, diameter_d, diameter_Dc,
									height_l, const_kx, const_ky );

	// Расчёт сопротивления катушки постоянному току в Омах.
	resistance = coil.coilResistance( resistivityConstant, length_l, diameter_dins );

	// Расчёт массы провода.
	weight = coil.coilWeight( specificGravity, length_l, diameter_d );

	// Вывод результатов расчёта.
	// TEST settings
	//cout << settings_calccoils->value("settings/value1",5).toString() << endl;

	cout << tr("number of calculation: ") << calcResultId << endl;
	cout << tr("number of the turns  W = ") <<  (double)number_w << ",\n";
	cout << tr("largest number of turns in a layer  Wl = ") <<  (double)(height_l/(diameter_d*const_ky)-1) << ",\n";
	cout << tr("number of layers  N = ") << ceil(number_w/(height_l/(diameter_d*const_ky)-1)) << ",\n";
	cout << tr("length of the  wire  l = ") << (double)length_l << (" ") << tr("m") << (",\n");
	cout << tr("resistance of the wire  R = ") << (double)resistance << (" ") << tr("Om") << (",\n");
	cout << tr("weight of the wire  m = ") << (double)weight*1000 << (" ") << tr("g") << (",\n");
	cout << tr("weight of the coil  M = ") << (double)(weight*1000+weight_coil) << (" ") << tr("g") << (",\n");
	cout << tr("thickness of the winding  c > ") << (double)thickness_c*1000 << (" ") << tr("mm") << (",\n");
	cout << tr("external diameter of the coil  De > ") << (double)(diameter_Dc+thickness_c*2)*1000 << (" ") << tr("mm") << ("\n");
	cout << separator << endl;

	calcResultId++;
}

// Очистить окно с результатами расчёта.
void mainWindow::on_pushButton_clear_clicked(bool checked) {
	ui.resultstBrowser->clear();
	calcResultId = 1;
}

// Вызов справки
void mainWindow::slotHelpContents() {
	QString fileName = QString("%1/help/CalcCoilsDoc_%2.pdf")
				.arg(QApplication::applicationDirPath())
				.arg(currLang);
	if(!QFile::exists(fileName)){
		QMessageBox::information(this, tr("Information"),
							tr("File 'CalcCoilsDoc_%1.pdf' not found!").arg(currLang),
							QMessageBox::Ok);
		return;
	}
	QDesktopServices::openUrl(QUrl("file:///" + fileName));
}

// Информация о разработчике.
void mainWindow::slotAboutProg() {
	Dialog dialog;
	dialog.exec();
}

mainWindow::~mainWindow (){
}
