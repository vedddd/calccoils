//============================================================================
// Name        : calcInductors.cpp
// Author      : Vetrov D.N.
// Version     : 0.01
// Copyright   : Your copyright notice
// Description : Calculation of the inductors in C++, Ansi-style
//============================================================================
#include "../include/calcInductors.h"

const long double PI = 3.1415926535897932384626433832795;
//===========================================================================
//	Расчёт числа витков цилиндрического соленоида по формуле Harolda Aldena Wheelera.
//===========================================================================
// diameter_d - толщина провода в метрах
// diameter_Dc - внутренний диаметр катушки в метрах
// height_l - ширина намотки катушки в метрах
// const_ky - коэффициент плотности намотки
// thickness_c - предполагаемая толщина намотки в метрах
// inductance_L - индуктивность в Генри
long double calcInductors::calcMultilayerInductors(long double diameter_d,
									long double diameter_Dc,
									long double height_l,
									long double const_kx,
									long double const_ky,
									long double *thickness_c,
									long double inductance_L) {

	//int i = 1;
	long double number_w = 0, // число витков многослойной катушки
		thickness_c_new = diameter_d/2.0, // уточнённая толщина намотки в метрах
		diameter_Da, // усреднённый диаметр катушки в метрах
		const_hx = const_kx*diameter_d,
		const_hy = const_ky*diameter_d;
	// Итерационный метод расчёта числа витков.
	while(fabs(*thickness_c-thickness_c_new)> 0.00000001){

		// Расчёт числа витков.
		*thickness_c = thickness_c_new;
		diameter_Da = diameter_Dc/2 + *thickness_c;
		number_w = (400.0/diameter_Da)*sqrt((inductance_L/PI)*(3*diameter_Da+9.0*height_l+10.0**thickness_c));

		// Уточнение толщины намотки.
		thickness_c_new = diameter_d+const_hx*const_hy*(number_w/(height_l-const_hy));
		//cout << i << "-я итерация, c = " << thickness_c_new*1000 << " мм," << endl;
		//i++;
	}
	*thickness_c = thickness_c_new;
	return number_w;
}

//===========================================================================
//	Рачёт длины провода(спирали) цилиндрической катушки в метрах.
//===========================================================================
// number_w - число витков многослойной катушки
// diameter_d - толщина провода в метрах
// diameter_Dc - внутренний диаметр катушки в метрах
// height_l - ширина намотки катушки в метрах
// const_ky - коэффициент плотности намотки
long double calcInductors::lengthWireCoil(long double number_w,
						   long double diameter_d,
						   long double diameter_Dc,
						   long double height_l,
						   long double const_kx,
						   long double const_ky) {

	long double length_l = 0, // длина провода в метрах
		lengthTurnsLastLayer = 0, // длина провода в последнем слое
		const_hx = const_kx*diameter_d,
		const_hy = const_ky*diameter_d,
		numberTurns = height_l/const_hy-1.0, // число витков в слое
		numberLayers = ceil(number_w/numberTurns);// число слоёв
	// Расчёт длины провода во всех слоях кроме последнего
	if (numberLayers>1) {
		for (long double n=0;n<=numberLayers-2;n++) {
			length_l += sqrt(const_hy*const_hy+pow(PI*(diameter_Dc+diameter_d+2*const_hx*n), 2));
		}
	} else length_l = 0;
	length_l = numberTurns*length_l;
	//cout << endl;
	//cout << "L(q-1) = " << length_l << " м," << endl;

	// Расчёт длины провода в последнем слое
	lengthTurnsLastLayer = (sqrt(const_hy*const_hy
						+pow(PI*(diameter_Dc+diameter_d+2*const_hx*(numberLayers-1)), 2)))
						*(number_w-numberTurns*(numberLayers-1));
	//cout << endl;
	//cout << "Длина провода последнем слое l = " << lengthTurnsLastLayer << " м," << endl;

	// Расчёт полной длины витков
	length_l += lengthTurnsLastLayer;

	return length_l;
}

//===========================================================================
//	Расчёт сопротивления катушки постоянному току в Омах.
//===========================================================================
// resistivityConstant - удельное сопротивление Ом*мм^2/м
// length_l - длина провода в метрах
// diameter_d - толщина провода в метрах
long double calcInductors::coilResistance(long double resistivityConstant,
										  long double length_l,
										  long double diameter_dins) {
	long double resistance = resistivityConstant*length_l/(PI*diameter_dins*diameter_dins*1000000/4);
	return resistance;
}

//===========================================================================
//	Расчёт веса провода катушки в килограммах.
//===========================================================================
// specificGravity - плотность провода кг/м^3
// length_l - длина провода в метрах
// diameter_d - толщина провода в метрах
long double calcInductors::coilWeight(long double specificGravity,
									  long double length_l,
									  long double diameter_d) {
	long double weight = specificGravity*length_l*PI*diameter_d*diameter_d/4;
	return weight;
}
