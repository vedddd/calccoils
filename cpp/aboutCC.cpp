#include <QDesktopServices>
#include <QUrl>
#include "../include/aboutCC.h"
#include "../include/TStranslator.h"

Dialog::Dialog() {
  ui.setupUi(this);

  this->setWindowTitle(tr("About CalcCoils"));

  QPixmap pixIcon;
  pixIcon.load(":images/icon.png");
  this->setWindowIcon(pixIcon);

  QPixmap pixItem;
  pixItem.load(":images/labelImage.png");
  ui.label_image->setPixmap(pixItem);

  // Информация о программе и разработчике
  ui.pushButton_web->setText("openlaboratory.ru");
  ui.label_version->setText("CalcCoil 0.0.7");
  ui.label_copyright->setText("Copyright (C) 2011-2013 Vetrov Dmitry");
  ui.label_textInfo1->setText("");
  ui.label_textInfo2->setText(tr("The program for calculation of inductors."));
  ui.label_textInfo3->setText(tr("CalcCoils is free software."));
}

// Ссылка на сайт разработчика
void Dialog::on_pushButton_web_clicked(bool checked) {
	QDesktopServices::openUrl(QUrl("http://openlaboratory.ru/calccoils/"));
}

// Закрыть окно
void Dialog::on_pushButton_exit_clicked(bool checked) {
	QMetaObject::invokeMethod(this, "close", Qt::QueuedConnection);
}
